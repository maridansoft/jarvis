package common;

/**
 * Common interface for all controller services.
 *
 * Clients implement this interface, Servers implement ControllerGrpc.Controller interface.
 */
public interface IExchangeController {

}
