// indicators

    indicator.analysisPicker{
        Some financial analytics post analysis results online, publish trough rss, those can either be parsed or entered manually...
    }
    
    indicator.blockchainTracker{
        This indicator is capable to do various different analysis on actual blockchains.
        For example it can find and track wallets which hold significant amount of cryptocoins and have been idle for years (dead wallets), as soon as some transaction happens from those wallets this indicator shows that...
        Or tracks wallets with high rate of transactions (most likely exchange wallets), compares volume of incoming and outgoing coins...
        Many different useful information can be harvested from blockchain, but to test its usefulness hypothesis must be tested with correlation indicator...

        [ requires block stream ] ( todo : bitcoinJ )
    }

    indicator.sleepAwake{
        Simply compare prices with a fixed time delay, or fixed trade quantity...

        [ requires trade stream ]
    }

    indicator.forceIndex{
        The FI is calculated by multiplying the difference between the last and previous closing prices by the volume of the commodity, yielding a momentum scaled by the volume.

        [ requires trade stream ]
    }

    indicator.adx{
        https://en.wikipedia.org/wiki/Average_directional_movement_index

        [ requires trade stream ]
    }

    indicator.coincube{
        Use a coincube account behaviour to track decisions that their system is making for its customers, a trading bot can simply replicate the system, but instead can use this information to improve existing strategies...

        [ requires dedicated event stream for account action events ]
    }

    indicator.userOpinion{
        this is a manually entered indicator, based on news and different rumors user may describe overall mood about market (value is in range -1, 1), if an operator has good insight in trading market, then strategies that use supervised learning can give more weight to this indicator over time

        [ requires a human being aware of the industry news, updates and generally community mood ]
    }

    indicator.whaleDetector{
        Monitors events for huge orders, trades, cancellations. Measures bull and bear powers in realtime.

        [ requires orderbook stream ]
    }
    
    indicator.storm{
        Tracks the market volatility, with this indicator you can tell if market is calm or volatile... Some strategies alike market making ones might do better being idle when its storming out there...

        [ requires trade and orderbook streams ]
    }
    
    indicator.googleTrends{
        Data from google trends correlates with trading volumes. Operator can create many instances of indicator with different keywords, like target currencies used in the market, or industries... then use correlation indicator to test how trend affects actual market...

        [ requires google trends data stream ] ( todo : googleTrends collector )
    }
    
    indicator.shazam {
        Shazam uses pattern recognition algorithms for songs, it splits songs to several second long identifiable patterns which are used to determine the song with just fraction of it (even with noise). The same approach might be useful for detecting patterns in charts, as this algorithm is capable to detect entire song with just small portion of sample and ignores the noise...

        [ requires database of predefined patterns ]
    }

    indicator.correlation {
        This indicator can compare two streams for correlation, can be configured for different offsets etc...

        [ requires two comparable streams ]
    }

    indicator.codeForce {
        This indicator tracks git activity of the project, for example one can track bitcoins git repository. This indicator shows how active the repository is...

        [ requires github event stream ] ( todo : githubCollector )
    }